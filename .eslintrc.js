module.exports = {
  root: true,
  env: {
    node: true,
  },
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    ecmaVersion: 2020,
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', 'vue'],
      },
    },
    'prettier-vue': {
      SFCBlocks: {
        template: true,
        script: true,
        style: true,
        customBlocks: {
          docs: { lang: 'markdown' },
          config: { lang: 'json' },
          module: { lang: 'js' },
          comments: false,
        },
      },
      usePrettierrc: true,
      fileInfoOptions: {
        ignorePath: '.testignore',
        withNodeModules: false,
      },
    },
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-recommended',
    'plugin:@typescript-eslint/recommended',
    '@vue/airbnb',
    '@vue/typescript/recommended',
    'plugin:storybook/recommended',
    'prettier',
  ],
  rules: {
    'no-param-reassign': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'import/prefer-default-export': 'off',
    'import/no-unresolved': [
      0,
      {
        caseSensitive: false,
      },
    ],
    'import/no-named-as-default': [0],
    'import/no-extraneous-dependencies': [
      0,
      {
        devDependencies: ['**/*.test.js', '**/*.spec.js'],
      },
    ],
    'operator-linebreak': 'off',
    'max-len': ['error', { code: 130 }],
    'vuejs-accessibility/label-has-for': ['off'],
    'vuejs-accessibility/aria-props': ['off'],
    'vuejs-accessibility/form-control-has-label': ['off'],
    'import/extensions': 'off',
    'arrow-body-style': ['off'],
    'no-shadow': 'off',
    'no-continue': 'off',
    'linebreak-style': 'off',
    'no-restricted-syntax': 'off',
    'import/export': 'off',
    'object-curly-newline': 'off',
    'no-spaced-func': 'off',
    'no-undef': 'off',
    'func-call-spacing': 'off',
  },
  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)', '**/tests/unit/**/*.spec.{j,t}s?(x)'],
      env: {
        jest: true,
      },
    },
  ],
};
