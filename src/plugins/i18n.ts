import { shallowRef, type App } from 'vue';
import enTranslations from '../i18n/en.json';

interface i18nOptions {
  locale: string;
}

type Translation = { [key: string]: string | number };

const translations = shallowRef<Translation>({});

const init = (options: i18nOptions) => {
  // TODO: add mulitple locales config and dynamic load
  // const translationLoaders: { [key: string]: any } = {
  //   en: () => import('../i18n/en.json').then((module) => module.default),
  // };
  const translationLoader: typeof enTranslations = enTranslations;

  const $t = (key: string): string | number => {
    if (translations.value[key] === undefined) {
      console.warn('Key ', key, ' not exist');

      return '';
    }

    return translations.value[key];
  };

  $t.setLocaleTranslations = (locale: string) => {
    // if (!translationLoaders[locale]) {
    //   console.warn('Locale not exist');

    //   return;
    // }

    // const newTranslations = translationLoaders[locale]();

    // translations.value = newTranslations;
    translations.value = translationLoader;
  };

  $t.setLocaleTranslations(options.locale);

  return $t;
};

export default {
  install(app: App, options: i18nOptions) {
    app.config.globalProperties.$t = init(options);

    app.provide('$t', app.config.globalProperties.$t);
  },
};
