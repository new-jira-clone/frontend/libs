import { i18n } from './i18n';

export {};

declare module '@vue/runtime-core' {
  export interface ComponentCustomProperties {
    $t: i18n;
  }
}
