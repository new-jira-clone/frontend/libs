export type i18n = (key: string) => string | number;
