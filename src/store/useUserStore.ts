import { ref, readonly, Ref, DeepReadonly, computed } from 'vue';
import { defineStore } from 'pinia';
import { IUser } from '@new-jira-clone/libs/src/types';

export const useUserStore = defineStore('user', () => {
  const user = ref<IUser | null>(null);
  const isInit = ref(false);

  const setUser = (newUser: IUser | null): IUser | null => {
    if (isInit.value === false) isInit.value = true;
    user.value = newUser;

    return user.value;
  };

  return {
    setUser,
    getUser: readonly(user) as DeepReadonly<Ref<IUser>>,
    isUserAuth: computed(() => !!user.value),
    isUserInit: computed(() => isInit.value),
  };
});
