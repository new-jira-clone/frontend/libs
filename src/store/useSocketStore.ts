import { shallowRef } from 'vue';
import { Socket } from 'socket.io-client';

const socket = shallowRef<Socket | null | undefined>(undefined);

export const useSocketStore = () => {
  const setSocket = (newSocket: Socket | null) => {
    socket.value = newSocket;
  };

  return {
    socket,
    setSocket,
  };
};
