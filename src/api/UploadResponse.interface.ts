export interface UploadResponse<T> {
  status: number;
  data: T;
}
