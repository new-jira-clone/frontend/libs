import api from './api';
import { UploadResponse } from './UploadResponse.interface';

export default {
  async uploadFile(form: FormData): Promise<UploadResponse<string>> {
    return api.post('/api/upload', form);
  },
};
