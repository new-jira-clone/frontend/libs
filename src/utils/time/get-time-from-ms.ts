export interface TimeUnits {
  seconds: number;
  minutes: number;
  hours: number;
}

export function convertMilliseconds(milliseconds: number): TimeUnits {
  const seconds = Math.round(milliseconds / 1000);
  const minutes = Math.round(milliseconds / (1000 * 60));
  const hours = Math.round(milliseconds / (1000 * 60 * 60));

  return {
    seconds,
    minutes,
    hours,
  };
}
