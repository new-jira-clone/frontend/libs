export const formatTo = (date: Date, format = 'HH:MM') => {
  const getHours = () => {
    if (`${date.getHours()}`.length === 1) {
      return `0${date.getHours()}`;
    }

    return date.getHours();
  };

  const getMinutes = () => {
    if (`${date.getMinutes()}`.length === 1) {
      return `0${date.getMinutes()}`;
    }

    return date.getMinutes();
  };

  if (format === 'HH:MM') {
    return `${getHours()}:${getMinutes()}`;
  }

  return '';
};
