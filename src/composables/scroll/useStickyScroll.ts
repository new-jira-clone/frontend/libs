export const useStickyScroll = () => {
  const moveScrollTo = (targetRef: Element | null, to: number) => {
    if (targetRef === null) return;

    targetRef.scrollTop = to;
  };

  const moveScrollToBotoom = (targetRef: Element | null) => {
    if (targetRef === null) return;

    moveScrollTo(targetRef, targetRef.scrollHeight);
  };

  const checkScrollIsBottom = (targetRef: Element | null, padding = 20): boolean => {
    if (targetRef === null) return true;
    if (targetRef.scrollTop === 0 && targetRef.scrollHeight <= targetRef.clientHeight) return true;

    return targetRef.scrollHeight - Math.floor(targetRef.scrollTop) - targetRef.clientHeight <= padding;
  };

  return {
    moveScrollTo,
    moveScrollToBotoom,
    checkScrollIsBottom,
  };
};
